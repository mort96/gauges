#!/usr/bin/env python3
import serial
import time

port = serial.Serial('/dev/ttyUSB0', 9600);

x = 0;
while True:
    while x < 255:
        port.write([x])
        x += 1
        time.sleep(0.01)
    while x > 0:
        port.write([x])
        x -= 1
        time.sleep(0.01)
