#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/sleep.h>

#define UART_BAUD 9600

#define PWM_A OCR2A
#define PWM_B OCR2B

volatile uint8_t c;

void pwm_init(void)
{
	// Set up timer output for timer 2
	//  Enable both A and B outputs for the timer, both in mode 2, which is
	//  set to 0 on timer compare match, set to 1 on timer reset, therefore,
	//  when the timer compare register is set to 255, the duty cycle will be
	//  100%, and when it's set to 0, the duty cycle will be 0%.
	TCCR2A = _BV(COM2A1) | _BV(COM2B1);
	
	//  Both outputs, OCR2A and OCR2B must be set to output mode
	DDRB |= _BV(PB3);
	DDRD |= _BV(PD3);

	// Set up timer for PWM
	//  Select wavegen mode 3, or Fast PWM mode. This increments the timer from
	//  0 to 255, then resets it and starts again.
	TCCR2A |= _BV(WGM20)  | _BV(WGM21);
	
	//  Choose clock select 0, this is just the CPU clock frequency of 16MHz
	TCCR2B = _BV(CS20);
}

void uart_init(void)
{
	// Set the baudrate
	//  Ths formula for calculating UBRR can be found in the datasheet
	UBRR0H = (F_CPU / UART_BAUD / 16 - 1) >> 8;
	UBRR0L = (F_CPU / UART_BAUD / 16 - 1)  & 0xff;

	// Set up other things
	//  Enable the UART reciever and the interrupt for a completed recieved
	//  byte.
	UCSR0B = _BV(RXEN0)  | _BV(RXCIE0);
	
	//  Choose UART character size 3, which is 8 bits per character
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);
}

ISR(USART_RX_vect)
{
	PWM_A = UDR0;
	PWM_B = UDR0;
}

uint8_t prev = 0;
int main()
{
	uart_init();
	pwm_init();
	ACSR |= _BV(ACD);
	PWM_A = 0;
	PWM_B = 0;
	sei();

	while (1) {
		sleep_mode();
	}

	return 0;
}
